"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const path = require("path");
const app = express();
const listen_port = process.env.PORT || 3000;
app.use(express.static(path.join(__dirname, '..', 'dist')));
app.listen(listen_port, () => {
    console.log(`App is listening on port ${listen_port}`);
});
