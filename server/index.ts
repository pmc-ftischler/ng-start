import * as express from 'express';
import * as path from 'path';
import * as compression from 'compression';

const app = express();
const listen_port = process.env.PORT || 3000;

app.use(compression());

app.use(express.static(path.join(__dirname, '..', 'dist')));

app.listen(listen_port, () => {
  console.log(`App is listening on port ${listen_port}`);
});
